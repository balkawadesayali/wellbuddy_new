@extends('layouts.app')
@section('content')
<body>

<div class="table-responsive">   
<table id="partner"  class="display res " style="width:100%">
      <thead class="thead">
      <tr>
        <th>Date</th>
        <th>Patient ID</th>
        <th>Patient Name</th>
        <th>Treatment</th>
        <th>DOA</th>
        <th>DOD</th>
        <th>Status</th>
        <!--added updated date-->
        <th>Updated Date</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
   
    </tbody>
  </table>
</div>

<!-- Admit model -->
            <!-- The Modal -->
                <div class="modal modalBg" id="admit_modal">
                    <div class="modal-dialog">
                        <div class="modal-content"> 
                            <!-- Modal Header -->
                            <div class="modal-header mheader">
                                <span class="modal-title mh-title" id="patient_name">Admit Patient</span>
                                <button type="button" style="color:#fff;" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <form role="form" id="admit_form" action="{{ url('admit') }}" method="post">
                            <input type="hidden" name="id" id="admit_id" value=""/>
                            <!-- @csrf -->
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="patient_id">Enter Patient Id*</label>
                                            <input  class="form-control inputheight rounded-3" type="text" name="patient_id" value="" placeholder="Enter patient id" />
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="enterdiscountcode">Enter Discount Code*</label>
                                            <input  class="form-control inputheight rounded-3" type="text" name="enterdiscountcode" value="" placeholder="Enter dicount code" />
                                            @error('enterdiscountcode')	
									        <span for="enterdiscountcode" class="error">{{ $message }}</span>
							            	@enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert-danger modal_error" style="padding: 0.5em; font-weight: bold; text-align: center;"> </div>   

                                        <div class="alert-success alert-dismissible response_alert" style="padding: 0.5em; font-weight: bold; text-align: center;">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="cutom_button_submit" name="admit" value="admit">Admit</button>
                                        </div>
                                    </div>
                                </div>      
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Discharge model -->
            <!-- The Modal -->
            <div class="modal fade modalBg" id="discharge">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header mheader">
                        <span class="mh-title"  id="discharge_patient_name">Discharge Patient </span>
                        <button type="button" style="color:#fff;"  class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->  
                        <form role="form" id="discharge_form" action="{{ url('discharge') }}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" id="discharge_id" value=""/>

                            <!-- @csrf -->
                            <div class="modal-body" id="my_modal">
                                <div class="row mb-0">
                                    <p><span class="fa fa-exclamation-circle"></span> Please enter the estimated bill details. This process should be done before the actual invoice is generated. Actual invoice will have to include Medic24x7 discount amount</p>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="totalAmount">Total bill:</label>
                                            <input id="totalAmount" class="form-control inputheight rounded-3" type="text" name="total_amount" value="" placeholder="Enter total amount" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="thirdPartyPayment" >TPA share:</label>
                                            <input id="thirdPartyPayment" class="form-control inputheight rounded-3" type="text" name="tpa_paid" value="" placeholder="Enter amount" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="customerPayment" >Customer share:</label>
                                            <input id="customerPayment" class="form-control inputheight rounded-3" type="text" name="customer_paid" value="" placeholder="Enter amount" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" id="datepicker" >
                                            <label for="dateofdischarge" class="mb-0">Date of Discharge:</label>
                                            <input id="dateofdischarge" class="form-control inputheight rounded-3" type="text" name="dateofdischarge" value="<?php echo date('d/m/Y'); ?>" placeholder="Select Date" />
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="paymentMode" class="mb-0">Payment Mode:</label>
                                            <div class="dropdown dropdownhieght">
                                                <select class="form-select modalDropdownhieght" name="payment_mode" aria-label="Default select example">
                                                    <option class="dropdownhieght" selected value="1">NEFT</option>
                                                    <option class="dropdownhieght" value="2">BANK Transfer</option>
                                                    <option class="dropdownhieght" value="3">UPI</option>
                                                    <option class="dropdownhieght" value="4">Credit / Debit Card</option>
                                                    <option class="dropdownhieght" value="5">Cash</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="mb-0">Upload Bill(PDF):</label>
                                            <div class="form-group files color">
                                                <input type="file" id="bill_pdf" name="bill_pdf" accept=".pdf" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="alert-danger disc_error" style="padding: 0.5em; text-align: center;"> </div>
                                <hr>
                                <div class="row">
                                    <?php $key=1; ?>
                                    @foreach($commissions as $commission)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="comments">{{ $commission->commission_type }}:</label>
                                            <input id="particulars{{ $key }}" type="hidden" name="particulars{{ $key }}" value="{{ $commission->commission_type }}" readonly/>
                                            <input id="amount{{ $key }}" class="form-control inputheight rounded-3" type="text" name="amount{{ $key }}" value="" placeholder="Enter amount" />
                                        </div>
                                    </div>
                                    <?php $key++; ?>
                                    @endforeach
                                    
                                    <input type="hidden" name="particulat_cnt" id="particulat_cnt" value="{{ $key-1 }}">
                                    

                                    <div class="col-md-12">
                                        <div class="alert-danger discharge_error" style="padding: 0.5em; font-weight: bold; text-align: center;"> </div>
                                        <div class="alert-success alert-dismissible discharge_response" style="padding: 0.5em; font-weight: bold; text-align: center;">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        </div>
                                    </div>
                                </div> 

                                <!--<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="comments">Comments(Optional):</label>
                                            <input id="comments" class="form-control inputheight rounded-3" type="text" name="comment" value="" placeholder="Enter Comments" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-right mb-4" style="margin-top:0px;">
                                    <input type="hidden" name="particulat_cnt" id="particulat_cnt" value="{{ $key+1 }}">
                                    <a href="#"><span class="generatediscountam" onclick="generate_disc();">Generate Discount Amount</span></a>
                                    --<a href="#"><span class="generatediscountam" onclick="add_more_particulars()">+ add more</span></a>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-5 mb-2">
                                        <div class="text-center mycart">
                                            <span class="cartHeader">Customer Discount:</span>
                                            <hr style="background-color:#e6e6e6;">
                                            <div class="row">
                                                <span class="cart-amount mt-0 patient_disc_amt">Rs. 1,000</span>
                                            </div>
                                            <input type="hidden" name="customer_disc_per" id="customer_disc_per" value=""/>
                                        </div>
                                        <div class="text-center mycartNA">
                                            <span class="cartHeader">Customer Discount: N/A</span>
                                        </div>
                                    </div>
                                </div>-->
                            </div>    
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <div class="col-md-12">
                                    <div class="row" style="padding: 0 20em 0 20em;">
                                        <div class="col-md-12 text-center">
                                            <p>Discount/Cashback to the customer</p>
                                        </div>
                                        
                                        <input type="hidden" name="customer_disc_per" id="customer_disc_per" value=""/>
                                        <div class="col-md-12 text-center">
                                            <input id="patient_disc_amt" class="form-control inputheight rounded-3 patient_disc_amt" type="text" name="patient_disc_amt" value="" placeholder="" readonly style="margin-bottom: -50px;"/>
                                            <button type="button" class="cutom_button_submit" onclick="generate_disc();" style="margin-bottom: 12px;margin-left: 10em;width: 40%;">Show</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="cutom_button_submit" name="discharge" value="discharge">Discharge</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center mt-2">
                                            <span class="cancel" data-dismiss="modal">Cancel</button>
                                        </div>    
                                    </div>
                                </div>      
                            </div>
                        </form>
                    </div>
                </div>                
            </div>
           
            </div>
            </div>
<!-- for data tables-->
  <!-- <script type="text/javascript" src=" https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  
  <script src="{{ asset('public/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{ asset('public/js/jquery.validate.min.js')}}"></script>
  <script src="{{ asset('public/js/datatables.bundle.js')}}" type="text/javascript"></script> -->

<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

<!-- date Picker-->
<script>
    $('#dateofdischarge').datepicker({
        uiLibrary: 'bootstrap4',
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
    $('.modal_error').hide();
    $('.response_alert').hide();

    $('.discharge_error').hide();
    $('.discharge_response').hide();
    $('.disc_error').hide();
    $('.mycart').hide();

    $("#partner").DataTable({
        responsive: !0,
        processing: !0,
        serverSide: !0,
        "pageLength": 10,
        "order": [[ 8, "desc" ]],
        "dom": 'Brfrtip',
            "buttons": [
                {
                    "extend": 'excel',
                    "text": '<button class="btn">Export<i class="customExcelIcon" style="padding-left:41px; font-size:26px;"></i></button>',
                    "titleAttr": 'Excel',
                    "action": newexportaction
                },
            ],
        language: { search: "" },
        "ajax": {
        "url": "{{url('all_patients')}}",
                "dataType": "json",
                "type": "POST",
                "data": function (d) {
                    d._token = "{{csrf_token()}}";
                },
                "deferLoading": 57,
                "initComplete": function (settings, json) {
                    // $(".preloader").fadeOut();
                }
        },
        columns: [//{data: "id"},
        {data: "created_at"},
        {data: "patient_id"},
        {data: "name"},
        {data: "treatment_needed"},
        {data: "date_of_admission"},
        {data: "date_of_discharge"},
        {data: "status"},
        {data: "updated_at"},
        {data: "action"}
        ],
        'columnDefs': [{
        'targets': [], // column index (start from 0)
                'orderable': true, // set orderable false for selected columns
                createdRow: function (row, data, dataIndex) {
                // Set the data-status attribute, and add a class
                }
        }]
    });
    function newexportaction(e, dt, button, config) {
         var self = this;
         var oldStart = dt.settings()[0]._iDisplayStart;
         dt.one('preXhr', function (e, s, data) {
             // Just this once, load all data from the server...
             data.start = 0;
             data.length = 2147483647;
             dt.one('preDraw', function (e, settings) {
                 // Call the original action function
                 if (button[0].className.indexOf('buttons-copy') >= 0) {
                     $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                     $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                     $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                     $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-print') >= 0) {
                     $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                 }
                 dt.one('preXhr', function (e, s, data) {
                     // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                     // Set the property to what it was before exporting.
                     settings._iDisplayStart = oldStart;
                     data.start = oldStart;
                 });
                 // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                 setTimeout(dt.ajax.reload, 0);
                 // Prevent rendering of the full data to the DOM
                 return false;
             });
         });
         // Requery the server with the new one-time export settings
         dt.ajax.reload();
     }
    $('.dataTables_filter input').attr("placeholder", "Search");
    $('#admit_form').validate({
        rules: {
            patient_id: 'required',
            enterdiscountcode: 'required',
        },
        messages: {
            patient_id: 'Patient id is required',
            enterdiscountcode: 'Discount code is required',
        },

        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    if(response=='success'){
                        //$('#admit_modal').modal('hide');
                        $('.modal_error').hide();
                        $('.response_alert').show();
                        $('.response_alert').html('Patient Admitted Successfully..!');
                        setTimeout(function () {
                            location.reload(true);
                        }, 2000);
                    }else{
                        $('.modal_error').show();
                        $('.modal_error').html(response);
                    }
                }            
            });
        }
    });

    $('#discharge_form').validate({
        rules: {
            total_amount: 'required',
            tpa_paid: 'required',
            customer_disc_per: 'required',
        },
        messages: {
            total_amount: 'Total amount is required',
            tpa_paid: 'TPA paid is required',
            customer_disc_per: 'Please generate discount',
        },

        submitHandler: function(form) {
            var form_data = new FormData($(form)[0]);
            $.ajax({
                url: form.action,
                dataType: 'text',  // <-- what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                type: form.method,
                data: form_data, //$(form).serialize(),
                success: function(response) {
                    if(response=='success'){
                        //$('#admit_modal').modal('hide');
                        $('.discharge_error').hide();
                        $('.discharge_response').show();
                        $('.discharge_response').html('Patient Discharged Successfully..!');
                        setTimeout(function () {
                            location.reload(true);
                        }, 2000);
                    }else{
                        $('.discharge_error').show();
                        $('.discharge_error').html(response);
                    }
                }            
            });
        }
    });

    $("#reload_table").click(function (e) {
        $('#partner').DataTable().draw(true);
    })
    
});
</script>

<!-- script to fetch id for discharge form -->

<script type="text/javascript">
function add_more_particulars(){
    var cnt = $('#particulat_cnt').val();
    cnt = parseInt(cnt)+1
    $(".particulars_div").append('<div class="col-md-6"><div class="form-group"><input id="particulars'+cnt+'" class="form-control inputheight rounded-3" type="text" name="particulars'+cnt+'" value="" placeholder="Enter Particulars" /></div></div><div class="col-md-6"><div class="form-group"><input id="amount'+cnt+'" class="form-control inputheight rounded-3" type="text" name="amount'+cnt+'" value="" placeholder="Enter amount" /></div></div>');
    $('#particulat_cnt').val(cnt);
}

function generate_disc(){
    var discharge_id = $("#discharge_id").val();
    var totalAmount = $("#totalAmount").val();
    var thirdPartyPayment = $("#thirdPartyPayment").val();
    var customerPayment = $("#customerPayment").val();

    if( totalAmount<=0 ){
        $('.disc_error').show();
        $('.disc_error').html('Total Amount Should be Greater Than 0');
    }else if(thirdPartyPayment <= 0){
        $('.disc_error').show();
        $('.disc_error').html('TPA Amount Should be Greater Than 0');
    }else if(customerPayment < 0){
        $('.disc_error').show();
        $('.disc_error').html('Customer Share Should be Greater Than or Equals To 0');
    }else if(parseInt(thirdPartyPayment) > parseInt(totalAmount)){
        $('.disc_error').show();
        $('.disc_error').html('TPA Amount Should Not be Greater Than Total Amount');
    }else{
        $.ajax({
            url: "{{ url('generate_disc') }}",
            type: 'post',
            data: $('#discharge_form').serialize(),//{discharge_id: discharge_id, totalAmount: totalAmount, thirdPartyPayment: thirdPartyPayment},
            success: function(response) {
                $('.disc_error').hide();
                $('.mycartNA').hide();
                //$('.patient_disc_amt').html('<i class="fa fa-inr" style="font-size: 25px; font-weight:100" aria-hidden="true"></i>'+response[1]);
                $('#patient_disc_amt').val(response[1]);
                $('.patient_disc_per').html('You Saved '+response[0]+'%!');
                $('.mycart').show();
            }            
        });
    }
}

function dischargeModel(id, name){
   // console.log(name);
    $("#discharge_id").val(id);
    $("#discharge_patient_name").text('Discharge Patient: '+name);
}

function admitModel(id,name){
    //console.log(name);
    $("#admit_id").val(id);
    $("#patient_name").text('Admit Patient: '+name);
} 
</script>

</body>
</html>
@stop