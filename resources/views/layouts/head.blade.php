<!DOCTYPE html>
<html lang="en">
  
<head>
    <title>Medic24x7</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="apple-touch-icon" href="public/assets/img/apple-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="public/images/icons/favicon.ico">

    <!-- Load Require CSS -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="public/assets/css/templatemo.css">

    <!-- Font CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Model -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- module css -->
    <link rel="stylesheet" href="public/bs-4.1.1/bootstrap-4-1-1.css">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-datepicker.css') }}">
    
    <!-- Datepicker -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- file upload -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Custom CSS --> 
    <link rel="stylesheet" href="public/assets/css/style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    
    <link rel="stylesheet" href="public/css/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Datatable CSS-->
    <link rel="stylesheet" href="public/css/datatable.css">
    <link rel="stylesheet" href="{{ asset('public/css/datatables.bundle.css')}}">

    <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  
    <script src="{{ asset('public/js/bootstrap-datepicker.js')}}"></script>
    <!-- <script src="{{ asset('public/js/jquery.validate.min.js')}}"></script> -->
    <script src="{{ asset('public/js/validate.js')}}"></script>
    <script src="{{ asset('public/js/datatables.bundle.js')}}" type="text/javascript"></script>
    <!-- <script type="text/javascript" src="jquery.js"></script>   -->
</head>