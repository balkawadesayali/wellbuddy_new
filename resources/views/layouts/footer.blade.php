<!-- Start Footer -->
<footer >
    <div class="container">
        <div class="row ">
            <div class="col-md-12 text-center">
                <p class="text-lg-start text-center footer light-300">
                    Copyright © <?php //echo date('Y'); ?> Medic 24/7 Ltd. All rights Reserved.
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->