@extends('layouts.app')
@section('content')
    <div class="com-md-12" style="margin-top:1em; margin-bottom:0px">
        <!-- @if($errors->any())
          
                {{$errors->first()}}
            </div>
		@endif -->

        <ul id="tabMenu" class="nav nav-pillsa">
            <li class="nav-item1 {{ (Session::get('tabProfile') === 'Profile'   || Session::get('tabBedsanddesposits') === 'Bedsanddesposits' || $errors->any())? '' : 'activeTab' }} {{ (count($selected_tpa_list)>0?'completed':'alertIcon') }}">
                <a href="#CashlessInsurananceTPA"  class="nav-link"  data-toggle="tab">Third Party Administrator(TPA)</a>
            </li>
            <li class="nav-item1 {{ (count($selected_insurance_list)>0?'completed':'alertIcon') }}">
                <a href="#Insurance" class="nav-link"  data-toggle="tab">Insurance</a>
            </li>
            <li class="nav-item1  {{ (count($selected_speciality_list)>0?'completed':'alertIcon') }}">
                <a href="#Speciallities"  class="nav-link" data-toggle="tab">Speciallities</a>
            </li>
            <li class="nav-item1  {{ (count($selected_doctor_list)>0?'completed':'alertIcon') }}">
                <a href="#Doctors"  class="nav-link" data-toggle="tab">Doctors</a>
            </li>
            <li id='bedsanddesposits' class=" nav-item1 {{ (Session::get('tabBedsanddesposits') === 'Bedsanddesposits' || $errors->any())? 'activeTab' : '' }}  {{ (count($partner_beds)>0?'completed':'alertIcon') }}">
                <a href="#Bedsanddesposits"  class="nav-link" data-toggle="tab">Beds and Deposits</a>
            </li>
            <li class="nav-item1 {{ (Session::get('tabProfile') === 'Profile')? 'activeTab' : '' }}  {{ (count($requests)>0?'completed':'alertIcon') }}">
                <a href="#Profile"  class="nav-link" data-toggle="tab">Profile</a>
            </li>
            <li class="nav-item1">
                <a href="#Biling"  class="nav-link" data-toggle="tab">Billing</a>
            </li>
        </ul>
    </div>
    <div class="col-md-12">
        <div id="tab-content-box" class="tab-content tab-content-box">
            <!-- CashlessInsurananceTPA -->
            <div class="tab-pane fade {{ (Session::get('tabProfile') === 'Profile'   || Session::get('tabBedsanddesposits') === 'Bedsanddesposits' || $errors->any())? '' : 'active show' }}" id="CashlessInsurananceTPA">
                <div class="row mb-0" style="height: 3%;">
                    <div class="col-md-6 col-sm-12 chips-center text-left" style="margin-top:3%;">
                        <span class="Add-Beds-and-deposit mt-4 chips-title">
                            Third Party Administrator(TPA)
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-12 chips-center mt-4">
                        <div class="ui-widget">
                            <div class="form-group">
                                <input id="tags" class="form-control inputheight search" type="text" placeholder="Enter TPA Name" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>    
                <hr class="mt-1" style="margin-left:15px; width: 96%;">

                 @foreach($selected_tpa_list as $tpa) 

                 <span class='btn btn-info1' id='<?php echo $tpa->tpa_name;?>' style='margin: 6px;'><?php echo $tpa->tpa_name;?>&nbsp;&nbsp; <span onclick="removeCompany('<?php echo $tpa->tpa_name;?>')" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>
                 @endforeach
                <div class="row">
                    <div class="col-md-12">
                        <div style="margin-left:10px;" id="companyName"></div>
                        <input type="hidden" name="companies" id="companies"/>
                    </div>
                </div>
            </div>

            <!-- Insurance -->
            <div class="tab-pane fade " id="Insurance">
                <div class="row mb-0" style="height: 3%;">
                    <div class="col-md-6 col-sm-12 col-sm-6 chips-center text-left" style="margin-top:3%;">
                        <span class="Add-Beds-and-deposit mt-4 chips-title">
                        Insurance
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-12 chips-center mt-4">
                        <div class="ui-widget">
                            <div class="form-group">
                                <input id="insuranceTags" class="form-control inputheight search" type="text" placeholder="Enter Insurance Name" autocomplete="off" />
                            </div>

                        </div>
                    </div>
                </div>

                <hr class="mt-1" style="margin-left:15px; width: 96%;">
                 @foreach($selected_insurance_list as $ins) 
                 <span class='btn btn-info1' id='<?php echo $ins->name_of_insurance;?>' style='margin: 6px;'><?php echo $ins->name_of_insurance;?>&nbsp;&nbsp; <span onclick="removeInsurance('<?php echo $ins->name_of_insurance;?>')" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>
                 @endforeach

                <div class="row">

                    <div class="col-md-12">
                        <div style="margin-left:10px;" id="insuranceName"></div>
                        <input type="hidden" name="insurance" id="insurance"/>
                    </div>
                </div>
            </div>

            <!-- Speciallities -->
            <div class="tab-pane fade" id="Speciallities">
                <div class="row mb-0" style="height: 3%;">
                    <div class="col-md-6 col-sm-12 col-sm-6 chips-center text-left" style="margin-top:3%;">
                        <span class="Add-Beds-and-deposit mt-4 chips-title">
                        Speciallities
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-12 chips-center mt-4">
                        <div class="ui-widget">
                            <div class="form-group">
                                <input id="speciallitiesTags" class="form-control inputheight search" type="text" placeholder="Enter Speciallities Name" autocomplete="off" />
                            </div>

                        </div>
                    </div>
                </div>
                
                <hr class="mt-1" style="margin-left:15px; width: 96%;">
                 @foreach($selected_speciality_list as $spl) 
                 <span class='btn btn-info1' id='<?php echo $spl->name;?>' style='margin: 6px;'><?php echo $spl->name;?>&nbsp;&nbsp; <span onclick="removeSpeciallities('<?php echo $spl->name;?>')" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>
                 @endforeach
                <div class="row">
                    <div class="col-md-12">
                        <div style="margin-left:10px;" id="speciallitiesName"></div>
                        <input type="hidden" name="speciallities" id="speciallities"/>
                    </div>
                </div>
            </div>

            <!-- Doctors -->
            <div class="tab-pane fade" id="Doctors">
                <div class="row mb-0" style="height: 3%;">
                    <div class="col-md-6 col-sm-12 col-sm-6 chips-center text-left" style="margin-top:3%;">
                        <span class="Add-Beds-and-deposit mt-4 chips-title">
                        Doctors
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-12 chips-center mt-4">
                        <div class="ui-widget">
                            <div class="form-group">
                                <input id="doctorsTags" class="form-control inputheight search" type="text" placeholder="Enter Doctors Name" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>                
                
                <hr class="mt-1" style="margin-left:15px; width: 96%;">
                 @foreach($selected_doctor_list as $doctor) 
                 <span class='btn btn-info1' id='<?php echo $doctor->doctor_name;?>' style='margin: 6px;'><?php echo $doctor->doctor_name;?>&nbsp;&nbsp; <span onclick="removeDoctors('<?php echo $doctor->doctor_name;?>')" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>
                 @endforeach
                <div class="row">
                    <div class="col-md-12">
                        <div style="margin-left:10px;" id="doctorsName"></div>
                        <input type="hidden" name="doctors" id="doctors"/>
                    </div>
                </div>
            </div>
            
            <!-- Bedsanddesposits -->
            <div class="tab-pane fade {{ (Session::get('tabBedsanddesposits') === 'Bedsanddesposits' || $errors->any())? 'active show' : '' }}" id="Bedsanddesposits">
                <div class="row">
                    <span class="Add-Beds-and-deposit">
                    Add Beds and Deposits
                    </span>
                </div>
                <div class="row">
                    @if ($message = Session::get('success'))
                    <span style="margin-left:4%; width: 90%;" class="alert alert-success"><i class="fa fa-check-circle fa-lg"></i>  {{ $message }}</span>
                    @endif
                </div>
                
                <hr class="mt-1" style="margin-left:15px; width: 96%;">          

                 <!-- Add new row dyanamically for Beds and desposits -->

                <div class="row bedsndeposits" style="">         
                    <form id="addbedsdepositsForm" class="form-horizontal" method="POST" action="{{ url('addbed') }}">
                    <input type="hidden" name="tabBedsanddesposits" value="Bedsanddesposits">
                        <div class="row">
                            {{csrf_field()}}   
                            <div class="table-responsive">
                                <table id="teamFields" class="table bedsDepotable" >
                                    <thead>
                                    <tr>
                                        <th class="tableTh">Bed Type*</th>
                                        <th class="tableTh">Total Beds*</th>
                                        <th class="tableTh">Available Beds</th>
                                        <th class="tableTh">Charge per day*</th>
                                        <th class="tableTh">Deposit*</th>
                                        <th class="tableTh">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($partner_beds) > 0)
                                            @foreach($partner_beds as $beds) 
                                            <tr>
                                                <td><select name="category_id[]" class="form-select inputheight" id="choice" onchange="getValue(this);"><option value="">Select Bed Type</option>@foreach($bed_type as $bed)<option value="{{ $bed->id }}"  @if($bed->id == $beds->category_id) selected="selected" @endif>{{ $bed->category }}</option>@endforeach</select></td>
                                                <td>
                                                    <input type="text" name="total_beds[]" id="total_beds" value="<?php echo $beds->total_beds;?>" class="form-control inputheight rounded-3" >
                                                    
                                                </td>
                                                <td>
                                                    <input type="text" name="beds_occupied[]" id="beds_occupied" value="<?php echo $beds->beds_occupied;?>" class="form-control inputheight rounded-3">
                                                    <span class="beds_error"></span>
                                                </td>
                                                <td><input type="text" name="price[]" id="price" value="<?php echo $beds->price;?>" class="form-control inputheight rounded-3"></td>
                                                <td><input type="text" name="deposite[]" id="deposite" value="<?php echo $beds->deposite;?>" class="form-control inputheight rounded-3"></td>
                                                <td><button type="button" class="btn btn-danger deleteRow" style="padding:12px; background-color:none;" onclick="removeBed('<?php echo $beds->id;?>')" ><i class="fa fa-trash" style="font-weight:100; color:#f00; " aria-hidden="true"></i></i></button></td>
                                                
                                            </tr>
                                            @endforeach
                                        @else
                                        <tr>
                                            <td><select name="category_id[]" class="form-select inputheight" id="category_id0" onchange="getValue(this);"><option value="">Please select bed type</option>@foreach($bed_type as $bed)<option value="{{ $bed->id }}" >{{ $bed->category }}</option>@endforeach</select></td>
                                            <td><input type="text" name="total_beds[]" id="total_beds0" value="" class="form-control inputheight rounded-3 total_beds" placeholder="Total beds"></td>
                                            <td><input type="text" name="beds_occupied[]" id="beds_occupied0" value="" class="form-control inputheight rounded-3 beds_occupied" placeholder="Available beds"></td>
                                            <td><input type="text" name="price[]" id="price0" value="" class="form-control inputheight rounded-3 price" placeholder="Charge per day"></td>
                                            <td><input type="text" name="deposite[]" id="deposite0" value="" class="form-control inputheight rounded-3 deposite" placeholder="Deposit">                                            </td>
                                            <td><button type="button" class="btn btn-danger deleteRow" style="padding:12px;"><i class="fa fa-trash" style="font-weight:100; color:#f00; " aria-hidden="true"></i></button></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>    
                        <div class="row text-right mt-1">
                            <a type="button" class="addmore" style="margin-bottom: 15px;" onclick="AddMore()"><span class="addmore"style="font-size:12px;">+ add more</span></a>
                        </div>    
                                     <!-- <div class="row text-right mt-5">
                                        <a type="button" href="#" onclick="AddMore()"><span class="addmore" style="font-size:14px;">+ add more</span></a>
                                    </div> -->
                        <div class="mt-3 mb-4 text-center ">
                            <input type="submit" class="cutom_button" />
                        </div>      
                    </form>
                </div>
            </div>

            <!-- Profile -->
             @foreach($requests as $request)
            <div class="tab-pane fade {{ (Session::get('tabProfile') === 'Profile')? 'active show' : '' }}" id="Profile">
                <div class="row">
                    <span class="Add-Beds-and-deposit">
                            Edit Profile
                    </span>
                </div>    
                <div class="row">
                    @if ($message = Session::get('profile'))
                    <span style="margin-left:4%; width: 90%;" class="alert alert-success"><i class="fa fa-check-circle fa-lg"></i>   {{ $message }}</span>
                    @endif
                </div>
                
                <hr class="mt-1" style="margin-left:15px; width: 96%;">

                <div class="container-fluid">
                        <form method="post" action="{{ url('updateUser') }}">
                            <input type="hidden" name="tabProfile" value="Profile">
                         {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 border-right">
                                <div class="form-group">
                                    <label for="hospitalName">Hospital Name*</label>
                                    <input id="hospitalName" class="form-control inputheight rounded-3" type="text" name="hospitalName" value="{{$request->title}} " placeholder="Enter hopital name" />
                                </div>
                                <div class="form-group">
                                    <label for="address">Address*</label>
                                    <input id="address" class="form-control inputheight rounded-3" type="text" name="address" value="{{$request->address}} "placeholder="Enter address" />
                                </div>
                                <div class="form-group">
                                    <label for="address2">Address 2</label>

                                    <input id="address2" class="form-control inputheight rounded-3" type="text" name="address2" value="{{$request->address2}} "placeholder="Address2" />

                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="country">Country*</label>

                                            <input id="country" class="form-control inputheight rounded-3" type="text" name="country" value="India" placeholder="Country" disabled />

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                           <label for="state" class="">State*</label>
                                            <select name="state_id" class="form-control dropdownhieght" id="state" style="height:calc(3rem + 2px)" required>
                                                <option value="0">Select State</option>
                                                @foreach($state as $states)
                                                    <option value="{{ $states->id }}"  @if($states->id == $request->state) selected="selected" @endif>{{ $states->state }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="achievedDate">City*</label>
                                            <select name="city_id" class="form-control dropdownhieght" id="city" style="height:calc(3rem + 2px)" required>
                                            <option value="0">Select City</option>
                                            @foreach($city as $cities)
                                                    <option value="{{ $cities->id }}"  @if($cities->id == $request->city) selected="selected" @endif>{{ $cities->city }}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                    </div>         
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mobileNumber">Mobile Number*</label>

                                            <input id="mobileNumber" class="form-control inputheight rounded-3" type="text" name="mobile" value="{{$request->mobile}} " placeholder="Mobile Number"/>


                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="emailId">Email Id*</label>
                                            <input id="emailId" class="form-control inputheight rounded-3" type="text" name="email" value="{{$request->email}} " placeholder="Email Id" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="location">Location*</label>
                                    <input class="form-control inputheight rounded-3 locationIcon" type="text" name="location_name" value="{{$request->location_name}}" id="search_input" placeholder="Type address..." />
                                    <input type="hidden" id="location" name="location" value="" />
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="enterGSTNumber">Enter GST Number*</label>
                                            <input id="enterGSTNumber" class="form-control inputheight rounded-3" type="text" name="gst" value="{{$request->gst_number}}" placeholder="Enter GST Number"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="zipCode">Zip Code*</label>

                                            <input id="zipCode" class="form-control inputheight rounded-3" type="text" name="zipCode" value="{{$request->pincode}}" placeholder="Zip Code" />
                                        </div>    
                                    </div>
                                </div>
                                
                                 <!-- @foreach($admin_person as $admin) -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="adminName">Admin Name*</label>
                                            <input id="adminName" class="form-control inputheight rounded-3" type="text" name="admin_name" value="{{$request->admin_name}}" placeholder="Admin Name" />
                                        </div>    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="contactName">Contact Number*</label>
                                            <input id="contactName" class="form-control inputheight rounded-3" type="text" name="admin_contact" value="{{$request->admin_contact}}" placeholder="contactName" />
                                        </div>
                                    </div>
                                </div>
                                <!-- @endforeach -->
                                <!-- <div class="row text-right">
                                    <a href="#"><span class="addmore">+ add more</span></a>
                                </div> -->
                                <!-- @foreach($billing_person as $billing) -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="billingPersonName">Billing Person Name*</label>
                                            <input id="billingPersonName" class="form-control inputheight rounded-3" type="text" name="billing_person_name" value="{{$request->billing_person_name}}" placeholder="Billing Person Name" />
                                        </div>    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="contactNumber" >Contact Number*</label>
                                            <input id="contactNumber" class="form-control inputheight rounded-3" type="text" name="billing_person_contact" value="{{$request->billing_person_contact}}" placeholder="Contact Number" />
                                        </div>
                                    </div>
                                </div>
                                <!-- @endforeach -->
                                <!-- <div class="row text-right">
                                    <a href="#"><span class="addmore">+ add more</span></a>
                                </div>     -->
                                
                            </div>
                        </div>  
                         @endforeach
                        <div class="mt-3 text-center ">
                             <input type="submit" name="update" class="cutom_button" value="Update" />
                        </div>
                            
                    </form>
                </div>
            </div>
            <!-- Billing section hear -->
            <div class="tab-pane fade" id="Biling">
                <div class="table-responsive" style="padding:2em;">   
                    <table id="partner" class="display res " style="width:100%; padding: 2em;">
                        <thead class="thead">
                        <tr>
                            <th>Date</th>
                            <th>Patient ID</th>
                            <th>Patient Name</th>
                            <th>Discount Code</th>
                            <th>Customer Discount</th>
                            <th>Bill Amount</th>
                            <th>TPA Paid</th>
                            <th>DOA</th>
                            <th>DOD</th>
                            <th>Updated Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>
<!-- js Part -->
<!-- for data tables-->
<!-- <script type="text/javascript" src=" https://code.jquery.com/jquery-3.5.1.js"></script> -->
<!-- 27-12-21-->
  <!-- <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  
  <script src="{{ asset('public/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{ asset('public/js/jquery.validate.min.js')}}"></script>
  <script src="{{ asset('public/js/datatables.bundle.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="jquery.js"></script>   -->
  <!-- end-->
<!-- <script src="{{ asset('public/js/datatables.bundle.js')}}" type="text/javascript"></script> -->
  
<script type="text/javascript">
    jQuery(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#partner").DataTable({
        responsive: !0,
        processing: !0,
        serverSide: !0,
        "pageLength": 10,
        "order": [[ 9, "desc" ]],
        "dom": 'Brfrtip',
            "buttons": [
                {
                    "extend": 'excel',
                    "text": '<button class="btn">Export<i class="customExcelIcon" style="padding-left:41px; font-size:26px;"></i></button>',
                    "titleAttr": 'Excel',
                    "action": newexportaction
                },
            ],
        
        language: { search: "" },
        "ajax": {
        "url": "{{url('discharged_patients')}}",
                "dataType": "json",
                "type": "POST",
                "data": function (d) {
                    d._token = "{{csrf_token()}}";
                },
                "deferLoading": 57,
                "initComplete": function (settings, json) {
                    // $(".preloader").fadeOut();
                }
        },
        columns: [{data: "created_at"},
        {data: "patient_id"},
        {data: "name"},
        {data: "discount_code"},
        {data: "discount_amount"},
        {data: "total_amount"},
        {data: "tpa_paid"},
        {data: "date_of_admission"},
        {data: "date_of_discharge"},
         {data: "updated_at"},
        {data: "bill_pdf"},
       
        ],
        'columnDefs': [{
        'targets': [], // column index (start from 0)
                'orderable': true, // set orderable false for selected columns
                createdRow: function (row, data, dataIndex) {
                // Set the data-status attribute, and add a class
                }
        }]
    });

    function newexportaction(e, dt, button, config) {
         var self = this;
         var oldStart = dt.settings()[0]._iDisplayStart;
         dt.one('preXhr', function (e, s, data) {
             // Just this once, load all data from the server...
             data.start = 0;
             data.length = 2147483647;
             dt.one('preDraw', function (e, settings) {
                 // Call the original action function
                 if (button[0].className.indexOf('buttons-copy') >= 0) {
                     $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                     $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                     $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                     $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-print') >= 0) {
                     $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                 }
                 dt.one('preXhr', function (e, s, data) {
                     // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                     // Set the property to what it was before exporting.
                     settings._iDisplayStart = oldStart;
                     data.start = oldStart;
                 });
                 // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                 setTimeout(dt.ajax.reload, 0);
                 // Prevent rendering of the full data to the DOM
                 return false;
             });
         });
         // Requery the server with the new one-time export settings
         dt.ajax.reload();
     }
    $('.dataTables_filter input').attr("placeholder", "Search");
    $("#reload_table").click(function (e) {
        $('#partner').DataTable().draw(true);
    })
});
</script>

<script type="text/javascript">
    var selected = [];

     var availableTags = new Array();
     var available_id = new Array();
    <?php foreach($tpa_list as $key => $val){ ?>
        availableTags.push('<?php echo $val->tpa_name;?>');
    <?php } ?>
  $( function() {
    $("#tags").autocomplete({
      source: availableTags,
      select: function( event, ui){
        var value = ui.item.value;
        selected.push(value);
        refreshcompanyDiv();
        var i = availableTags.indexOf(value);
        availableTags.splice(i, 1);
        event.preventDefault();
        $("#tags").focusout();
        $("#tags").val('');
      }
    });
  });


  
  function refreshcompanyDiv(){
    $("#companies").val(selected.join(','));

  
      var companies_html = selected.map(function(f, i){
          $.ajax({
                    type: 'POST',
                    url: "{{ url('add_tpa') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : f,                    
                    },
                    dataType: "json",
                });

        return "<span class='btn btn-info1' id='"+f+"' style='margin: 6px;'>"+f+"&nbsp;&nbsp; <span onclick=\"removeCompany('"+f+"')\" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>";

    });
      $("#companyName").html(companies_html);
  }
  
  function removeCompany(companies){
    //availableTags.push(companies);
  //  console.log(companies);
  //   

     $.ajax({
                    type: 'POST',
                    url: "{{ url('remove_tpa') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : companies,                    
                    },
                   
                    dataType: "json",

                    
                });
 
    var x = document.getElementById(companies);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
      var i = selected.indexOf(companies);
      //console.log("i : "+i);
      selected.splice(i, 1);

      refreshcompanyDiv();
  }
</script>

<script>
    $('#dateofdischarge').datepicker({
        uiLibrary: 'bootstrap4',
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
</script>

<script>
    $(document).ready(function(){
        $('a[data-toggle="tab"]').on("shown.tab", function(e){
            var activeTab = $(e.target).text(); // Get the name of active tab
            var previousTab = $(e.relatedTarget).text(); // Get the name of previous active tab
            $(".active-tab span").html(activeTab);
            $(".previous-tab span").html(previousTab);
        });

            $('.nav-item1').click(function() {
            $('.nav-item1.activeTab').removeClass('activeTab');
            $(this).addClass('activeTab');

            // $('.nav-item1.completed').removeClass('completed');
            // $(this).addClass('completed');
            // $('.nav-item1').addClass('alert');
            });
    });
</script>

<!-- Speciallities -->
<script>
    var selectedSpeciallities = [];

    var availableSpeciallities = new Array();
    <?php foreach($speciality_list as $key => $val){ ?>
        availableSpeciallities.push('<?php echo $val->name;?>');
    <?php } ?>
  $( function() {
    $("#speciallitiesTags").autocomplete({
      source: availableSpeciallities,
      select: function( event, ui){
        var value = ui.item.value;
        selectedSpeciallities.push(value);
        speciallitiesDiv();
        var i = availableSpeciallities.indexOf(value);
        availableSpeciallities.splice(i, 1);
        event.preventDefault();
        $("#speciallitiesTags").focusout();
        $("#speciallitiesTags").val('');
      }
    });
  });
  
  function speciallitiesDiv(){
    $("#speciallities").val(selectedSpeciallities.join(','));
      var speciallities_html = selectedSpeciallities.map(function(f, i){
          $.ajax({
                    type: 'POST',
                    url: "{{ url('add_speciality') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : f,                    
                    },
                   
                    dataType: "json",

                    
                });

        return "<span class='btn btn-info1' id='"+f+"' style='margin: 6px;'>"+f+"&nbsp;&nbsp; <span onclick=\"removeSpeciallities('"+f+"')\" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>";
    });
      $("#speciallitiesName").html(speciallities_html);
  }
  
  function removeSpeciallities(speciallities){
    availableSpeciallities.push(speciallities);
    $.ajax({
                    type: 'POST',
                    url: "{{ url('remove_speciality') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : speciallities,                    
                    },
                   
                    dataType: "json",

                    
                });
 
    var x = document.getElementById(speciallities);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
      var i = selectedSpeciallities.indexOf(speciallities);
      selectedSpeciallities.splice(i, 1);
      speciallitiesDiv();
  }
</script>

<!-- Doctors -->
<script>
    var selectedDoctors = [];
    var availableDoctors = new Array();
    <?php foreach($doctor_list as $key => $val){ ?>
        availableDoctors.push('<?php echo $val->doctor_name;?>');
    <?php } ?>
  $( function() {
    $("#doctorsTags").autocomplete({
      source: availableDoctors,
      select: function( event, ui){
        var value = ui.item.value;
        selectedDoctors.push(value);
        refreshDoctorsDiv();
        var i = availableDoctors.indexOf(value);
        availableDoctors.splice(i, 1);
        event.preventDefault();
        $("#doctorsTags").focusout();
        $("#doctorsTags").val('');
      }
    });
  });
  
  function refreshDoctorsDiv(){
    $("#doctors").val(selectedDoctors.join(','));
      var doctors_html = selectedDoctors.map(function(f, i){
        $.ajax({
                    type: 'POST',
                    url: "{{ url('add_doctor') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : f,                    
                    },
                   
                    dataType: "json",

                    
                });
        return "<span class='btn btn-info1' id='"+f+"' style='margin: 6px;'>"+f+"&nbsp;&nbsp; <span onclick=\"removeDoctors('"+f+"')\" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>";
    });
      $("#doctorsName").html(doctors_html);
  }
  
  function removeDoctors(Doctors){
    availableDoctors.push(Doctors);
    $.ajax({
                    type: 'POST',
                    url: "{{ url('remove_doctor') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : Doctors,                    
                    },
                   
                    dataType: "json",

                    
                });
    var x = document.getElementById(Doctors);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
      var i = selectedDoctors.indexOf(Doctors);
      selectedDoctors.splice(i, 1);
      refreshDoctorsDiv();
  }
</script>

<!--  Insurance -->
<!-- Doctors -->
<script type="text/javascript">
    var selectedInsurance = [];
 
    var availableInsurance = new Array();
    <?php foreach($insurance_list as $key => $val){ ?>
        availableInsurance.push('<?php echo $val->name_of_insurance;?>');
    <?php } ?>
    
  $( function() {
    $("#insuranceTags").autocomplete({
      source: availableInsurance,
      select: function( event, ui){
        var value = ui.item.value;
        selectedInsurance.push(value);
        refreshInsuranceDiv();
        var i = availableInsurance.indexOf(value);
        availableInsurance.splice(i, 1);
        event.preventDefault();
        $("#insuranceTags").focusout();
        $("#insuranceTags").val('');
      }
    });
  });
  
  function refreshInsuranceDiv(){
    $("#Insurance").val(selectedInsurance.join(','));
      var Insurance_html = selectedInsurance.map(function(f, i){
        $.ajax({
                    type: 'POST',
                    url: "{{ url('add_insurance') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : f,                    
                    },
                   
                    dataType: "json",

                    
                });
        return "<span class='btn btn-info1' id='"+f+"' style='margin: 6px;'>"+f+"&nbsp;&nbsp; <span onclick=\"removeInsurance('"+f+"')\" style='color:#333333;font-size:16px;'><img src='/medic24x7/public/images/icons/close-chip.png'></span></span>";
    });
      $("#insuranceName").html(Insurance_html);
  }
  
  function removeInsurance(Insurance){
    availableInsurance.push(Insurance);
      
       $.ajax({
                    type: 'POST',
                    url: "{{ url('remove_insurance') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : Insurance,                    
                    },
                   
                    dataType: "json",

                    
                });
 var i = selectedInsurance.indexOf(Insurance);
    var x = document.getElementById(Insurance);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
      selectedInsurance.splice(i, 1);
      refreshInsuranceDiv();
  }
</script>

<!-- script for add more - Beds and deposits -->
<script>
   var j = 1; 
  function AddMore(){

     var data='<tr>';
          data +=  '<td><select name="category_id[]" class="form-select inputheight" id="category_id'+ j +'" onchange="getValue(this);"><option value="">Please select bed type</option>@foreach($bed_type as $bed)<option value="{{ $bed->id }}" >{{ $bed->category }}</option>@endforeach</select></td>';
          data += '<td><input type="text" name="total_beds[]" value="" id="total_beds'+ j +'" class="form-control inputheight rounded-3 total_beds" placeholder="Total bed"></td>';
          data += '<td><input type="text" name="beds_occupied[]" value="" id="beds_occupied'+ j+'" class="form-control inputheight rounded-3 beds_occupied" placeholder="Available bed"></td>';
          data += '<td><input type="text" name="price[]" value="" id="price'+ j +'"  class="form-control inputheight rounded-3 price" placeholder="Charge per day"></td>';
          data += '<td><input type="text" name="deposite[]" value="" id="deposite;'+ j +'"  class="form-control inputheight rounded-3 deposite" placeholder="Deposit"></td>';
          data += '<td><button type="button" class="btn btn-danger deleteRow" style="padding:12px;"><i class="fa fa-trash" style="font-weight:100; color:#f00; " aria-hidden="true"></i></button></td>';
          data += '</tr>';
          $('#teamFields tr:last').after(data);
          j++;
  }
  $(document).ready(function(){
    
    $("#teamFields").on('click','.deleteRow',function(){
        $(this).parent().parent().remove();
    });

    //all validation of beds and deposits
    $.validator.addMethod("category_id_validation", function (value, element) {
        var flag = true;        
      $("[name^=category_id]").each(function (i, j) {
            $(this).parent('td').find('label.error').remove();
            $(this).parent('td').find('label.error').remove();                        
            if ($.trim($(this).val()) == '') {
                flag = false;
                $(this).parent('td').append('<label  id="category_id'+i+'-error" class="error">Please select Bed type</label>');
            }
        });             
        return flag;
    }, "");
    $.validator.addMethod("total_bed_validation", function (value, element) {
        var flag = true;        
      $("[name^=total_beds]").each(function (i, j) {
            $(this).parent('td').find('label.error').remove();
            $(this).parent('td').find('label.error').remove();                        
            if ($.trim($(this).val()) == '') {
                flag = false;
                $(this).parent('td').append('<label  id="total_beds'+i+'-error" class="error">Enter total beds</label>');
            }
        });             
        return flag;
    }, "");
    $.validator.addMethod("beds_occupied_validation", function (value, element) {
        var flaga = true;        
      $("[name^=beds_occupied]").each(function (i, j) {
            $(this).parent('td').find('label.error').remove();
            $(this).parent('td').find('label.error').remove();                        
            if ($.trim($(this).val()) == '') {
                flaga = false;
                $(this).parent('td').append('<label  id="beds_occupied'+i+'-error" class="error">Enter available beds</label>');
            }
        });             
        return flaga;
    }, "");
    $.validator.addMethod("price_validation", function (value, element) {
        var flaga = true;        
      $("[name^=price]").each(function (i, j) {
            $(this).parent('td').find('label.error').remove();
            $(this).parent('td').find('label.error').remove();                        
            if ($.trim($(this).val()) == '') {
                flaga = false;
                $(this).parent('td').append('<label  id="price'+i+'-error" class="error">Enter charge per bed</label>');
            }
        });             
        return flaga;
    }, "");
    $.validator.addMethod("deposite_validation", function (value, element) {
        var flaga = true;        
      $("[name^=deposite]").each(function (i, j) {
            $(this).parent('td').find('label.error').remove();
            $(this).parent('td').find('label.error').remove();                        
            if ($.trim($(this).val()) == '') {
                flaga = false;
                $(this).parent('td').append('<label  id="deposite'+i+'-error" class="error">Enter deposit</label>');
            }
        });             
        return flaga;
    }, "");
    $("#addbedsdepositsForm").validate({
        ignore: '',
        rules: {
            "total_beds[]": {
                total_bed_validation:true,
                number:true
            },
            "beds_occupied[]": {
                beds_occupied_validation:true,
                number:true
            },
            "price[]": {
                price_validation:true,
                number:true
            },
            "deposite[]": {
                deposite_validation:true,
                number:true
            },
            "category_id[]": {
                category_id_validation:true,
            },
            
        },
        submitHandler: function () {
            form.submit();
            return false;
    }
});
});

function removeBed(bed){
    availableInsurance.push(bed);
      
       $.ajax({
                    type: 'POST',
                    url: "{{ url('remove_bed') }}",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        'id' : bed,                    
                    },
                   
                    dataType: "json",

                    
                });
       $(this).parent().parent().remove();
  }
</script>

<script type="text/javascript">
    $("select").change(function()
 {
     var tr = $(this).closest("tr");
        tr.find('.cat option').attr("disabled",""); //enable everything

     //collect the values from selected;
     var  arr = $.map
     (
        tr.find('.cat option:selected'), function(n)
         {
              return n.value;
          }
      );

    //disable elements
    tr.find('.cat option').filter(function()
    {

        return $.inArray($(this).val(),arr)>-1; //if value is in the array of selected values
     }).attr("disabled","disabled");   

});
</script>
<!-- script for add more - Admin and contacts -->
<script>
  function AddMoreadmin(){

     var data='<tr>';
          data += '<td><input type="text" name="category_id" id="category_id"  class="form-control inputheight rounded-3"></td>';
          data += '<td><input type="text" name="total_beds" id="total_beds"  class="form-control inputheight rounded-3"></td>';
          
         /*
          data += '<td><input type="text" name="names[]" id="names" class="form-control col-md-4"></td>'; 
          data += '<td><input type="file" name="images[]" id="images"  class="form-control col-md-4"></td>';*/
          data += '<td><button type="button" class="btn btn-danger deleteRow">Delete</button></td>';
          data += '</tr>';
          $('#addadmincontact tr:last').after(data);
  }
  $(document).ready(function(){
    $("#addadmincontact").on('click','.deleteRow',function(){
        $(this).parent().parent().remove();
    });
}); 
</script>
<script type="text/javascript">
      $('#state').on('change',function(){
  var stateID = $(this).val();  
  if(stateID){
    $.ajax({
      type:"GET",
      url:"{{url('getCity')}}?state_id="+stateID,
      success:function(res){        
      if(res){
        $("#city").empty();
        $("#city").append('<option>Select City</option>');
        $.each(res,function(key,value){
          $("#city").append('<option value="'+key+'">'+value+'</option>');
        });
      
      }else{
        $("#city").empty();
      }
      }
    });
  }else{
    $("#city").empty();
  }
    
  });
</script>

<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script-->
<!-- Google Maps JavaScript library -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyB3xnnkjQ_TI1AKy1UFSF3pT72ydex_jTE"></script>
<script>
var searchInput = 'search_input';
$(document).ready(function () {
var autocomplete;
autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
//types: ['geocode'],
});
google.maps.event.addListener(autocomplete, 'place_changed', function () {
var near_place = autocomplete.getPlace();
var location = autocomplete.getPlace().geometry.location;
document.getElementById('location').value = location;
     // console.log(autocomplete.reviews);

});
});
</script>
<script type="text/javascript">
   
function getValue(option) {
    var catId = option.value;  
  if(catId){
    $.ajax({
      type:"GET",
      url:"{{url('getbed')}}?cat_id="+catId,
      success:function(res){        
      if(res>0){
        option.value=0;
        window.alert("Bed Type Already Added");
      }
      }
    });

}
}
</script>

<!-- Tab redirecting --> 
<!-- <script>
    $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function(event) {
        //alert('dddd');
        let activeTab = $(event.target), // activated tab
        id = activeTab.attr('href'); // active tab href

        // set id in html5 localstorage for later usage      
        localStorage.setItem('activeTab', id);
});

window.onload = function() {
   let tabID = localStorage.getItem('activeTab');
   $(tabID).tab('show');
 };

</script> -->
<script>
        //redirect to specific tab
        // $(document).ready(function () {
        //     $('#tabMenu a[href="#{{ old('tabBedsanddesposits') }}"]').tab('show');
        //     $('#tabMenu a[href="#{{ old('tabProfile') }}"]').tab('show');
        // });
</script>
@endsection