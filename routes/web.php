<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Partners\LoginController;
use App\Http\Controllers\Partners\PartnerprofileController;
use App\Http\Controllers\Partners\PatientController;
use App\Http\Controllers\website;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/partner', [LoginController::class, 'showLoginForm']);
Route::get('/partner/login', [LoginController::class, 'showLoginForm'])->name('partner.login');
Route::post('/partner/checklogin',[LoginController::class,'checklogin'])->name('partner.checklogin');

Route::group(['middleware' => 'partner'], function () {

    Route::get('/home', function () {
        return view('home');
    });
    Route::get('/manageHospital', function () {
        return view('managehopital');
    });
    Route::get('/home',[LoginController::class,'successlogin']);
    Route::get('/logout',[LoginController::class,'logout']);

    Route::get('/profile', [PartnerprofileController::class, 'userProfile']);
    Route::post('/updateUser', [PartnerprofileController::class, 'updateUser']);

    Route::post('/addbed', [PartnerprofileController::class, 'add_bed']);
    Route::post('/remove_bed', [PartnerprofileController::class, 'remove_bed']);

    Route::post('/add_tpa', [PartnerprofileController::class, 'add_tpa']);

    Route::get('home', [PatientController::class, 'index']);

    Route::post('admit', [PatientController::class, 'admit_patient']);
    Route::post('all_patients', [PatientController::class, 'patient_data']);

    Route::post('discharge', [PatientController::class, 'discharge_patient']);
    Route::post('generate_disc', [PatientController::class, 'generate_disc']);
    Route::post('discharged_patients', [PartnerprofileController::class, 'discharged_data']);

    Route::post('/remove_tpa', [PartnerprofileController::class, 'remove_tpa']);

    Route::post('/add_insurance', [PartnerprofileController::class, 'add_insurance']);
    Route::post('/remove_insurance', [PartnerprofileController::class, 'remove_insurance']);


    Route::post('/add_speciality', [PartnerprofileController::class, 'add_speciality']);
    Route::post('/remove_speciality', [PartnerprofileController::class, 'remove_speciality']);


    Route::post('/add_doctor', [PartnerprofileController::class, 'add_doctor']);
    Route::post('/remove_doctor', [PartnerprofileController::class, 'remove_doctor']);

    Route::get('/getCity', [PartnerprofileController::class,'getCity']);

    Route::post('/add_tpa', [PartnerprofileController::class, 'add_tpa']);

    Route::get('home', [PatientController::class, 'index']);
    Route::get('/getbed', [PartnerprofileController::class,'getbed']);
});
