<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StatecityController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FavouriteController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SubscriptionPlanController;
use App\Http\Controllers\AdvertisementController;
/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

// Route::middleware('auth:api')->get('/users', function (Request $request) {
//     return $request->users();
// });

Route::group(['prefix' => 'v1'], function () {
       

        Route::group(['prefix' => 'user'], function () { //customer
            Route::post('/login', [AuthController::class, 'login']);
            Route::post('/verifyotp', [AuthController::class, 'verifyotp']);
            Route::post('/register', [AuthController::class, 'register']);
        
            Route::get('/state/list', [StatecityController::class, 'stateList']);
            Route::get('/city/list/{id}', [StatecityController::class, 'cityList']);
            Route::get('/countryList', [StatecityController::class, 'countryList']);
        
            Route::group(['middleware' => ['assign.guard:app_api', 'jwt.verify']], function () {

            Route::get('/profile', [AuthController::class, 'userProfile']);
            Route::post('/me', [AuthController::class, 'userProfile']);
            Route::put('/user/update', [AuthController::class, 'updateUser']);
            Route::post('/logout', [AuthController::class, 'logout']);
            Route::post('/refresh', [AuthController::class, 'refresh']);

            Route::get("/favouriteList", [FavouriteController::class, 'favouriteList']); 
            Route::post("/addFavourite", [FavouriteController::class, 'addFavourite']);
            Route::post("/removeFavourite", [FavouriteController::class, 'addFavourite']);

           Route::post('/subscribe', [SubscriptionController::class, 'create']);
           Route::post('/verify_subscription', [SubscriptionController::class, 'update']);
           
  
            Route::post('/upload', [UploadController::class, 'store']);
            Route::resource('/upload', UploadController::class);

            Route::get('/viewSubCategory/{id}', [CategoriesController::class, 'viewSubCategory']);

           Route::post('/createRating', [ReportController::class, 'createRating']);
           Route::get('/plan_details', [SubscriptionPlanController::class, 'plan_details']);

           Route::post('/createAdvertisement', [AdvertisementController::class, 'createAdvertisement']);
            Route::get('/viewAdvertisement/{id}', [AdvertisementController::class, 'viewAdvertisement']);
            Route::get('/advertisementList', [AdvertisementController::class, 'advertisementList']);
            Route::post('/deleteAdvertisement/{id}', [AdvertisementController::class, 'deleteAdvertisement']);
            Route::get('/fetchUserAdvertisement', [AdvertisementController::class, 'fetchUserAdvertisement']);
            
            });
        });
});