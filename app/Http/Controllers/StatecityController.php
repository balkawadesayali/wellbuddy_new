<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\States;
use Validator;
use  Illuminate\Support\Facades\DB;

class StatecityController extends ApiController
{

public function countryList(Request $request) {
        

 $states = DB::table('countries')->orderby('name');
 $total = $states->get()->count();

    
            $temp = $states->get();
          

       return ApiController::apiCollection($temp,$total);

}

    
 public function stateList(Request $request) {
        
  $country_id=$request->country_id;
 $states = DB::table('states')->where('country_id',$country_id)->orderby('name');
 $total = $states->get()->count();

    
            $temp = $states->get();

          foreach ($temp as $value) {
            // $value->city_name=[];
            // $value->city_id=[];
            $value->city=[];
                $dt=$value->id;
               // echo $dt;exit;
                        if (!empty($dt) && !is_null($dt)) {
                       $query=DB::table('cities')->select('name','id')->where('state_id',$dt)->get();
                        foreach($query as $key=>$val)
                        {
                        $city_name[$key]=$val->name;
                        $city_id[$key]=$val->id; 
                        $value->city=array_combine($city_id,$city_name);  
                        }   
            }

          }

       return ApiController::apiCollection($temp,$total);

    }

  
     public function cityList(Request $request,$state_id) {
   
     $states = DB::table('cities')->where('state_id',$state_id)->orderby('city');
     $total = $states->get()->count();
     $temp = $states->get();  

       return ApiController::apiCollection($temp,$total);

    }
}