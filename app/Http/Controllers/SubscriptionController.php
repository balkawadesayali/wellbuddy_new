<?php

namespace App\Http\Controllers;

/**
 * Description of SubscriptionController
 *
 * @author sayali
 */
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Razorpay\Api\Api;
use App\Models\Subscription;

class SubscriptionController extends Controller {

    private $request;
    private $api;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->api = new Api("rzp_live_q6ZkNqLXJ5xr1s","VaeKD6B5LJP7MMdaaNf7OPqT");
    }
       
    public function create(Request $request) 
        {
        $user= response()->json(auth('app_api')->user());
        $array = json_decode(json_encode($user), true);
        $user_id=$array['original']['id'];
        $user_phone=$array['original']['user_mobile'];

        $plan=$request->plan_id;
        $query=DB::table('plans')->select('price','number_of_days')->where('id',$plan)->first();
        $price=$query->price;
        $duration=$query->number_of_days;
    
        $today=date('Y-m-d h:m:s');

         $expiry_date= date('Y-m-d h:m:s',strtotime('+'.$duration.'days',strtotime($today))) . PHP_EOL; 

                    $order = $this->api->order->create(array('receipt' => 'WELL','amount' => $price * 100, 'currency' => 'INR'));
                  //  print_r($order);exit;
                    $subsription= new Subscription();
                    $subsription->transaction_id = $order->id;
                    $subsription->user_id= $user_id;
                    $subsription->amount= $price;
                    $subsription->user_phone= $user_phone;
                    $subsription->payment_status=$order->status;
                    $subsription->is_active = 0;
                    $subsription->start_date= $today;
                    $subsription->end_date = $expiry_date;
                    $subsription->plan_id = $plan;
                    $subsription->save();


                  $data=array(
            'transaction_id'=>$order->id,
            'amount'=>$order->amount,
            'user_id'=>$user_id,
            'payment_status'=>$order->status,
           );

                        return response()->json([
                    'statusCode' => 200,
                    'message' =>'Data retrieval successfully',
                    'data' => $data,
                  
                        ], 200);

        }


    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update() {
        
            $order = $this->api->order->fetch($this->request->order_id);
            if($order->status=='paid' || $order->status=='Paid')
            {
            $subsription = Subscription::where('transaction_id', $order->id)->first();
            $sub_id= $subsription->id;
            $affected = DB::table('subscriptions')
              ->where('id',$sub_id)
              ->update(['is_active' => 1,'payment_status'=>$order->status]);
            return response()->json([
                    'statusCode' => 200,
                    'message' =>'Subscription purchased successfully',
                    'payment_status' => 'Paid'
                  
                        ], 200);
           }else{

             return response()->json([
                    'statusCode' => 422,
                    'message' =>'Subscription purchased failed',
                  
                        ], 422);
           }
           
        
    }

    

}
