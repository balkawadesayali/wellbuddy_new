<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\States;
use Validator;
use  Illuminate\Support\Facades\DB;
use App\Models\Upload;
use App\Models\Categories;


class SubscriptionPlanController extends ApiController
{
   public function __construct(Request $request) {
        $this->request = $request;
    }

  //view
  public function plan_details(Request $request) {

        $ads_category = DB::table('ads_category')->get();
        foreach($ads_category as $val){
            $val->features=[];
            $val->plan_details=[];
            $sub_category = DB::table('features')->where('cat_id',$val->id)->get();
            $val->features=$sub_category;
           
            $plans = DB::table('plans')->where('cat_id',$val->id)->get();
            $val->plan_details=$plans;

        }
         


 $total = $ads_category->count();

    
    
   if (array_key_exists('start', $this->request->all()) && !is_null($this->request->input('start'))) {
            $offset = $this->request->input('start');
            if (!$this->request->input('limit') || empty($this->request->input('limit'))) {
                $limit = 10;
            } else {
                $limit = $this->request->input('limit');
            }


            $ads_category->offset($offset)->limit($limit);
            $temp = $ads_category->get();
        } else {

            $temp = $ads_category;


        }



     
       return ApiController::apiCollection($temp,$total);
       }
     

}
