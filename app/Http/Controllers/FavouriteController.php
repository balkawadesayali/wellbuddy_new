<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Models\Favourite;
use App\Models\ApplicationUser;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  Illuminate\Support\Facades\DB;

class FavouriteController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    /** showing favouriteList  */
    public function favouriteList(Request $request)
    {
        $customer_id = $request->customer_id;
        if($customer_id=='')
        {
            $msg="Customer id not found";
            return ApiController::apiNotfound($msg);  
        }

        $favourite = DB::table('favourite')->select('*')->where('customer_id',$customer_id)->where('is_favourite',1)->get();

        //Count FavouriteList
        $total = $favourite->count();

        $advertisementsIdArray = array();    
        
        foreach($favourite as $value)
        {
            array_push($advertisementsIdArray, $value->advertisements_id);
        }
        $favouriteAdvertisements = DB::table('advertisements')
                                 ->join('categories', 'advertisements.category_id', '=', 'categories.category_id')
                                   ->select('advertisements.*', 'categories.name as category_name','categories.parent_id', 'categories.logo_id','categories.is_enabled')
                                   ->wherein('advertisements.id',$advertisementsIdArray)
                                   ->get();
        return ApiController::apiCollection($favouriteAdvertisements, $total);
    }

    /** add Favourite  function here*/
    public function addFavourite(Request $request)
    {
        //Validation Rules 
        $rules = [
                  'advertisements_id' => 'required',
                  'customer_id' => 'required',
                  'is_favourite' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        //Check validation here
        if($validator->fails()){
          return ApiController::apiValidate($validator->errors());
        }

        if (Favourite::where('advertisements_id',$request->advertisements_id)->where('customer_id',$request->customer_id)->exists())
        {
            $favourite = Favourite::where('advertisements_id',$request->advertisements_id)->where('customer_id',$request->customer_id)->first();
            $favouriteId = $favourite->id;

            //Find is_favouite and updating 
            $checkIsfavourite = Favourite::find($favouriteId);
            $checkIsfavourite->is_favourite = is_null($request->is_favourite) ? $checkIsfavourite->is_favourite : $request->is_favourite;
            $checkIsfavourite->save();

            if($request->is_favourite == "1")
            {
                $msg="Added successfully";
            }
            else
            {
                $msg="Removed successfully";
            }
            return response()->json(
                [
                    "statusCode" => 201,
                    "message" => $msg,
                    "data" => $checkIsfavourite
                ], 200);
        }
        else
        {
            //If not in favourite list
            $isFavourite = new Favourite;
            $isFavourite->advertisements_id = $request->advertisements_id;
            $isFavourite->customer_id = $request->customer_id;
            $isFavourite->is_favourite = $request->is_favourite;
            $isFavourite->save();

            if($request->is_favourite == "1")
            {
                $msg="Added successfully";
            }
            else
            {
                $msg="Removed successfully";
            }
            return response()->json(
                [
                    "statusCode" => 201,
                    "message" => $msg,
                    "data" => $isFavourite
                ], 200);
        }   
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function show(Favourite $favourite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favourite $favourite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favourite $favourite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favourite $favourite)
    {
        //
    }
}
