<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\States;
use Validator;
use  Illuminate\Support\Facades\DB;
use App\Models\Upload;
use App\Models\Categories;


class CategoriesController extends ApiController
{
   public function __construct(Request $request) {
        $this->request = $request;
    }
    //insert
    public function createCategory(Request $request) {

        $rules=[
            'name' => 'required',
            'parent_id'=>'required',
            'is_enabled'=>'required'
        ];

$validator=Validator::make($request->all(),$rules);
if($validator->fails()){
   return ApiController::apiValidate($validator->errors());
}

$logo=$request->logo_id;
        $categories = new Categories;
    $categories->name = $request->name;
    $categories->parent_id=$request->parent_id;
    $categories->logo_id=$request->logo_id;
    $categories->is_enabled=$request->is_enabled;
    $categories->save();

    return ApiController::apiCreated($categories);
  }
  //view
  public function viewSubCategory(Request $request,$id) {

    $search=$request->input('search');
    $orderr=$request->input('order');
    if($orderr=='')
    {
        $order='ASC';
    }else
    {
        $order=$orderr;
    }
  //echo $id;exit;
 if($id==0){
        $cat_details = DB::table('categories')->where('is_deleted','=',0)->where('parent_id',0)->get();
	foreach($cat_details as $val){
            $val->sub_category=[];
            $sub_category = DB::table('categories')->where('is_deleted','=',0)->where('parent_id',$val->id)->get();
            $val->sub_category=$sub_category;
            foreach($sub_category as $cat){
            $cat->product=[];
            $prod_category = DB::table('categories')->where('is_deleted','=',0)->where('parent_id',$cat->id)->get();
            $cat->sub_category=$prod_category;
            }

        }
         
       }


       else {
        $cat_details = Categories::where('parent_id', $id);

      }

 $total = $cat_details->count();

    
    
   if (array_key_exists('start', $this->request->all()) && !is_null($this->request->input('start'))) {
            $offset = $this->request->input('start');
            if (!$this->request->input('limit') || empty($this->request->input('limit'))) {
                $limit = 10;
            } else {
                $limit = $this->request->input('limit');
            }


            $cat_details->offset($offset)->limit($limit);
            $temp = $cat_details->get();
        } else {

            $temp = $cat_details;


        }

        foreach ($temp as $value) {
$value->parent_name='';
            $parent = Categories::where('id', $value->id)->get();
            if (!empty($parent) && !is_null($parent)) {
                $parent_name = [];
                foreach ($parent as $image) {
                      if($image->parent_id==0)
                        {
                          $name='Main';
                        }
                    else if (!empty($image->parent_id) && !is_null($image->parent_id)) {
                        $path = Categories::select('name as parent_name')->where('id', $image->parent_id)->whereNull('deleted_at')->first();
                      $name=$path->parent_name;
                       
                    }
                    $value->parent_name = $name;
                }   

}
        
}

foreach ($temp as $value) {
  $value->cat_img='';
              $catImage = Categories::where('id', $value->id)->get();
              if (!empty($catImage) && !is_null($catImage)) {
                  $cat_img = [];
                  foreach ($catImage as $image) {
                      if (!empty($image->logo_id) && !is_null($image->logo_id)) {
                          $path = Upload::select('path')->where('id', $image->logo_id)->first();
                         $value->cat_img = asset('/' . $path->path);
                      }
                  }   
  
  }
          
  }
     
       return ApiController::apiCollection($temp,$total);
       }
     public function viewCategory($id) {
     
        if (Categories::where('id', $id)->exists()) {
        $temp = Categories::where('id', $id)->get();
        foreach ($temp as $value) {
  $value->cat_img='';
              $catImage = Categories::where('id', $value->id)->get();
              if (!empty($catImage) && !is_null($catImage)) {
                  $cat_img = [];
                  foreach ($catImage as $image) {
                      if (!empty($image->logo_id) && !is_null($image->logo_id)) {
                          $path = Upload::select('path')->where('id', $image->logo_id)->first();
                         $value->cat_img = asset('/' . $path->path);
                      }
                  }   
  
  }
}
       return ApiController::apiItem($temp);

        } else {
       $message="No Data Found ";
           return ApiController::apiValidate( '',$message);
      }
    
  }
//     public function viewCategory($id) {
  
//       $data = array();
//               $Categories = Categories::where('id',$id)->orderBy('name', 'asc')->get();
//               $total = $Categories->count();
//               foreach ($Categories as $value) {
      
//                   $catImage = Categories::where('id', $value->id)->get();
//                   if (!empty($catImage) && !is_null($catImage)) {
//                       $source_img = [];
//                       foreach ($catImage as $image) {
//                           if (!empty($image->logo_id) && !is_null($image->logo_id)) {
//                               $path = Upload::select('path')->where('id', $image->logo_id)->first();
//                               $source_img[]['path'] = asset('/' . $path->path);
//                           }
//                       }
      
//                       $value->source_imgs = $source_img;    
//       $data[] = $value;
//       }
              
//       }
      
//       if(sizeof($data)>0)
//       {
//        return ApiController::apiItem($data);
//       }else {
//          $message="No Data Found";
//                  return ApiController::apiValidate( '',$message);
//       }
       
//    }   
      
      
// //update
  public function updateCategory(Request $request, $id) {

 $user = DB::table('categories')->where('id', $id)->first();

  //  print_r($user);exit;
    if (Categories::where('id', $id)->exists()) {
        $categories = Categories::find($id);
        $categories->name = is_null($request->name) ? $categories->name : $request->name;
        $categories->parent_id = is_null($request->parent_id) ? $categories->parent_id : $request->parent_id;
        $categories->logo_id = is_null($request->logo_id) ? $categories->logo_id : $request->logo_id;  
        $categories->is_enabled = is_null($request->is_enabled) ? $categories->is_enabled : $request->is_enabled;  
        $categories->save();

        return ApiController::apiItem($categories);

        } else {
       $message="No Data Found";
           return ApiController::apiValidate( '',$message);
        
    }
}

//delete
  public function deleteCategory ($id) {
  
      if(Categories::where('id', $id)->exists()) {
        $news_delete= Categories::where('id',$id)->update(['is_deleted' => 1]);
        $categories = Categories::find($id);
        $categories->delete();

         $cat = Categories::select('id')->where('parent_id', $id)->get()->toArray();
          $categories=Categories::whereIn('id', $cat)->delete();

         return ApiController::apiDeleted($categories);

      } else {
        $message="Category Not Found";
           return ApiController::apiNotfound($message);
      }
    }


}
