<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advertisements;
use Validator;
use  Illuminate\Support\Facades\DB;
use App\Models\Upload;
use App\Models\Categories;


class AdvertisementController extends ApiController
{
   public function __construct(Request $request) {
        $this->request = $request;
    }
    //insert
    public function createAdvertisement(Request $request) {

//         $rules=[
//             'name' => 'required',
//             'parent_id'=>'required',
//             'is_enabled'=>'required'
//         ];

// $validator=Validator::make($request->all(),$rules);
// if($validator->fails()){
//    return ApiController::apiValidate($validator->errors());
// }


       
if(isset($request->id))
{
    $id=$request->id;
   // $user = DB::table('advertisements')->where('id', $id)->first();
    // if(Categories::where('id', $id)->exists()) {
    if(Advertisements::where('id',$id)->exists()){
    $categories = Advertisements::find($id);
    $categories->category_id =  is_null($request->category_id) ? $categories->category_id : $request->category_id;
    $categories->subcategory= is_null($request->subcategory) ? $categories->subcategory : $request->subcategory;
    $categories->brand_name= is_null($request->brand_name) ? $categories->brand_name : $request->brand_name;
    $categories->title= is_null($request->title) ? $categories->title : $request->title;
    $categories->description= is_null($request->description) ? $categories->description : $request->description;
    $categories->price = is_null($request->price) ? $categories->price : $request->price;
    $categories->negotiable= is_null($request->negotiable) ? $categories->negotiable : $request->negotiable;
    $categories->product_status= is_null($request->product_status) ? $categories->product_status : $request->product_status;
    $categories->physical_condition= is_null($request->physical_condition) ? $categories->physical_condition : $request->physical_condition;
    $categories->purchased_date= is_null($request->purchased_date) ? $categories->purchased_date : $request->purchased_date;
    $categories->bill_available= is_null($request->bill_available) ? $categories->bill_available : $request->bill_available;
    $categories->user_firstname= is_null($request->user_firstname) ? $categories->user_firstname : $request->user_firstname;
    $categories->user_lastname= is_null($request->user_lastname) ? $categories->user_lastname : $request->user_lastname;
    $categories->mail_id= is_null($request->mail_id) ? $categories->mail_id : $request->mail_id;
    $categories->phone_no= is_null($request->phone_no) ? $categories->phone_no : $request->phone_no;
    $ar=implode(',', $request->logo);
   
    $categories->logo= is_null($request->logo) ? $categories->logo : $ar=implode(',', $request->logo);

     $categories->driven_km= is_null($request->driven_kmdriven_km) ? $categories->driven_km : $request->driven_km;
     $categories->phone_no= is_null($request->number_of_owner) ? $categories->number_of_owner : $request->number_of_owner;
     $categories->registration_state= is_null($request->registration_state) ? $categories->registration_state : $request->registration_state;
     $categories->house_type= is_null($request->house_type) ? $categories->house_type : $request->house_type;
     $categories->bedroom= is_null($request->bedroom) ? $categories->bedroom : $request->bedroom;
     $categories->construction_status= is_null($request->construction_status) ? $categories->construction_status : $request->construction_status;
     $categories->total_area= is_null($request->total_area) ? $categories->total_area : $request->total_area;
     $categories->carpet_area= is_null($request->carpet_area) ? $categories->carpet_area : $request->carpet_area;
     $categories->building_floors= is_null($request->building_floors) ? $categories->building_floors : $request->building_floors;
     $categories->floor_number= is_null($request->floor_number) ? $categories->floor_number : $request->floor_number;


    $categories->save();
    return ApiController::apiItem($categories);

        } else {
       $message="No Data Found";
           return ApiController::apiValidate( '',$message);


}

}
        $categories = new Advertisements;
    $categories->category_id = $request->category_id;
    $categories->subcategory=$request->subcategory;
    $categories->brand_name=$request->brand_name;
    $categories->title=$request->title;
    $categories->description=$request->description;
    $categories->price=$request->price;
    $categories->negotiable=$request->negotiable;
    $categories->product_status=$request->product_status;
    $categories->physical_condition=$request->physical_condition;
    $categories->purchased_date=$request->purchased_date;
    $categories->bill_available=$request->bill_available;
    $categories->user_firstname=$request->user_firstname;
    $categories->user_lastname=$request->user_lastname;
    $categories->mail_id=$request->mail_id;
    $categories->phone_no=$request->phone_no;

    $categories->driven_km=$request->driven_km;
    $categories->number_of_owner=$request->number_of_owner;
    $categories->registration_state=$request->registration_state;
    $categories->house_type=$request->house_type;
    $categories->bedroom=$request->bedroom;
    $categories->construction_status=$request->construction_status;
    $categories->total_area=$request->total_area;
    $categories->building_floors=$request->building_floors;
    $categories->floor_number=$request->floor_number;
      


    $ar=implode(',', $request->logo);
    $categories->logo=$ar;


    $categories->save();

    return ApiController::apiCreated($categories);
  }
  //view
  public function viewAdvertisement(Request $request,$id) {

    $search=$request->input('search');
    $orderr=$request->input('order');
    if($orderr=='')
    {
        $order='ASC';
    }else
    {
        $order=$orderr;
    }


        $cat_details = DB::table('advertisements')->where('is_deleted','=',0)->where('id',$id)->get();
      


 $total = $cat_details->count();

    
    
   if (array_key_exists('start', $this->request->all()) && !is_null($this->request->input('start'))) {
            $offset = $this->request->input('start');
            if (!$this->request->input('limit') || empty($this->request->input('limit'))) {
                $limit = 10;
            } else {
                $limit = $this->request->input('limit');
            }


            $cat_details->offset($offset)->limit($limit);
            $temp = $cat_details->get();
        } else {

            $temp = $cat_details;


        }

        foreach ($temp as $value) {
$value->category='';
$value->subcategory_name='';
            $category = Categories::where('id', $value->category_id)->get();
            $subcategory = Categories::where('id', $value->subcategory)->get();
            if (!empty($category) && !is_null($category)) {
                foreach ($category as $cat) {
                    $value->category= $cat;
                    }
                 
                }   

                 if (!empty($subcategory) && !is_null($subcategory)) {
                foreach ($subcategory as $sub) {
                    $value->subcategory_name= $sub;
                    }
                 
                }  

}
        


foreach ($temp as $value) {
  $value->adv_images='';
              $catImage = Advertisements::where('id', $value->id)->get();
              if (!empty($catImage) && !is_null($catImage)) {
                  $adv_images = [];
                  foreach ($catImage as $image) {
                 // /   print_r($image->logo);exit;
                      if (!empty($image->logo) && !is_null($image->logo)) {
                        
                        $array = explode(',', $image->logo);
                       for($i=0;$i<=sizeof($array)-1;$i++)
                       {
                          $path = Upload::select('path')->where('id', $array[$i])->first();
                           $value->adv_images = asset('/' . $path->path);
                          //$arr[]=$path->path;
                          //print_r($path);exit;
                       } 
                         
                        
                      }
                  }   
  
  }
          
  }
     
       return ApiController::apiCollection($temp,$total);
       }

  public function advertisementList(Request $request) {

    $search=$request->input('search');
    $orderr=$request->input('order');
    if($orderr=='')
    {
        $order='ASC';
    }else
    {
        $order=$orderr;
    }


        $cat_details = DB::table('advertisements')->where('is_deleted','=',0); //->get();
      


 $total = $cat_details->count();

    
    
   if (array_key_exists('start', $this->request->all()) && !is_null($this->request->input('start'))) {
            $offset = $this->request->input('start');
            if (!$this->request->input('limit') || empty($this->request->input('limit'))) {
                $limit = 10;
            } else {
                $limit = $this->request->input('limit');
            }


            $cat_details->offset($offset)->limit($limit);
            $temp = $cat_details->get();
        } else {

            $temp = $cat_details->get();


        }

        foreach ($temp as $value) {
$value->category='';
$value->subcategory_name='';
            $category = Categories::where('id', $value->category_id)->get();
            $subcategory = Categories::where('id', $value->subcategory)->get();
            if (!empty($category) && !is_null($category)) {
                foreach ($category as $cat) {
                    $value->category= $cat;
                    }
                 
                }   

                 if (!empty($subcategory) && !is_null($subcategory)) {
                foreach ($subcategory as $sub) {
                    $value->subcategory_name= $sub;
                    }
                 
                }  

}
        


foreach ($temp as $value) {
  $value->adv_images='';
              $catImage = Advertisements::where('id', $value->id)->get();
              if (!empty($catImage) && !is_null($catImage)) {
                  $adv_images = [];
                  foreach ($catImage as $image) {
                 // /   print_r($image->logo);exit;
                      if (!empty($image->logo) && !is_null($image->logo)) {
                        
                        $array = explode(',', $image->logo);
                       for($i=0;$i<=sizeof($array)-1;$i++)
                       {
                          $path = Upload::select('path')->where('id', $array[$i])->first();
                           $value->adv_images = asset('/' . $path->path);
                          //$arr[]=$path->path;
                          //print_r($path);exit;
                       } 
                         
                        
                      }
                  }   
  
  }
          
  }
     
       return ApiController::apiCollection($temp,$total);
       }     
   

//delete
  public function deleteAdvertisement ($id) {
  
      if(Advertisements::where('id', $id)->exists()) {
        $adv_delete= Advertisements::where('id',$id)->update(['is_deleted' => 1]);
       // $adv_delete->save();

        

         return ApiController::apiDeleted($adv_delete);

      } else {
        $message="Advertisement Not Found";
           return ApiController::apiNotfound($message);
      }
    }

    //List of users advertisement 
    public function fetchUserAdvertisement(Request $request)
    {
        $customer_id=$request->customer_id;

        if($customer_id=='')
        {
            $msg="Customer id not found";
            return ApiController::apiNotfound($msg);  
        }

        $usersAdvertisements = DB::table('advertisements')->select('*')->where('customer_id',$customer_id)->get();
        $total = $usersAdvertisements->count();

        return ApiController::apiCollection($usersAdvertisements, $total);
    }


}
